package CustomLayout;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.souha.testinglayout.R;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeParser;

import java.awt.font.NumericShaper;
import java.sql.Array;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import Persistance.Station;
import Persistance.TripLine;
import Persistance.TripLineStation;
import Utils.ParsingManager;
import ViewModels.LineViewModel;
import ViewModels.StationsViewModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by souha on 23/03/2018.
 */

public class CustomAdapter extends BaseAdapter {
    Context context;
    ArrayList<LineViewModel> items = new ArrayList<>();
    Realm realm;
    static int[] colors;

    public CustomAdapter(Context context, ArrayList<LineViewModel> items) {
        this.context = context;
        this.items = items;
        realm = Realm.getDefaultInstance();
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public LineViewModel getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        LineViewModel item = items.get(i);
        if (item != null)
            return item.getLineId();
        return -1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.station_card, viewGroup, false);
        }

        final LineViewModel s = (LineViewModel) this.getItem(i);

        TextView propTxt = (TextView) view.findViewById(R.id.station_name);
        MaterialLetterIcon myLetterIcon = (MaterialLetterIcon) view.findViewById(R.id.station_icon);
        String id = Integer.toString(s.getLineId());
        String name = s.getLineName();
        propTxt.setText(name);
        myLetterIcon.setLetter(name);
        if (CustomAdapter.colors == null || CustomAdapter.colors.length == 0)
            CustomAdapter.colors = view.getResources().getIntArray(R.array.letterShapeColors);
        int randomAndroidColor = colors[new Random().nextInt(colors.length)];
        myLetterIcon.setShapeColor(randomAndroidColor);
        // Make Toast For Click
        // Prompt a Dialog
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Try To Show List View Inside A Dialog
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
                Date currentDate = Calendar.getInstance().getTime();
                DateTime myDate = new DateTime(currentDate).withYear(2018).withMonthOfYear(1).withDayOfMonth(1);


                builderSingle.setTitle("Liste des horaires");
                int stationId = s.getLineId();

                RealmResults<TripLineStation> tripLineStations =
                        realm.where(TripLineStation.class)
                                .equalTo("tripLine.line.lineId", stationId)
                                .equalTo("tripLine.typeJour.typeJourId",1)
                                .sort("heurePassage")
                                .findAll();
                // Fill My Map
                Map<Integer,TripLine> tripLines = new HashMap<>();
                Map<Integer,List<TripLineStation>> tripMap = new HashMap<>();
                int currentMinId = 0;
                long currentMinDiff = 1000000000;
                for (TripLineStation tripLineStation:
                     tripLineStations) {
                    int key =tripLineStation.getTripLine().getTripLineId();
                    DateTime tripLineDate = new DateTime(tripLineStation.getHeurePassage());
                    long diff = myDate.getMillis() - tripLineDate.getMillis();
                    if (Math.abs(diff) < currentMinDiff){
                        currentMinId = key;
                        currentMinDiff = diff;
                    }
                    List<TripLineStation> currentList = new ArrayList<>();
                    if (tripMap.containsKey(key)){
                        currentList = tripMap.get(key);
                        currentList.add(tripLineStation);
                    }
                    else{
                        currentList.add(tripLineStation);
                        tripMap.put(key,currentList);
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        tripLines.putIfAbsent(key,tripLineStation.getTripLine());
                    }
                }
                // get The closest value


                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1);
                List<TripLineStation> results = new ArrayList<>();
                DateTimeFormatter dtf = DateTimeFormat.forPattern("HH:mm:ss");
                results = tripMap.get(currentMinId);
                if (results != null){
                    for (TripLineStation tripLineStation:
                            results) {
                        DateTime currenntHour = new DateTime(tripLineStation.getHeurePassage());
                        arrayAdapter.add(tripLineStation.getStation().getDesignationFr() + "    " + dtf.print(currenntHour));
                    }
                }else{
                    arrayAdapter.add("PAS D'HORAIRE");
                }
                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do Nothing
                    }
                });
                AlertDialog myDialog = builderSingle.create();
                myDialog.setCanceledOnTouchOutside(false);
                myDialog.show();
            }
        });

        return view;
    }
}
