package CustomLayout;

import android.widget.Filter;

import java.util.ArrayList;

import Persistance.Line;
import ViewModels.LineViewModel;

/**
 * Created by souha on 21/04/2018.
 */

public class PreferenceFilter extends Filter {
    ArrayList<LineViewModel> filterList;
    PreferenceAdapter adapter;

    public PreferenceFilter(ArrayList<LineViewModel> filterList, PreferenceAdapter adapter) {
        this.filterList = filterList;
        this.adapter = adapter;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();

        //VALIDATION
        if(constraint != null && constraint.length()>0)
        {

            //CHANGE TO UPPER FOR CONSISTENCY
            constraint=constraint.toString().toUpperCase();

            ArrayList<LineViewModel> filteredLines=new ArrayList<>();

            //LOOP THRU FILTER LIST
            for(int i=0;i<filterList.size();i++)
            {
                //FILTER
                if(filterList.get(i).getLineName().toUpperCase().contains(constraint))
                {
                    filteredLines.add(filterList.get(i));
                }
            }

            results.count=filteredLines.size();
            results.values=filteredLines;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.items= (ArrayList<LineViewModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
