package CustomLayout;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.souha.testinglayout.R;
import com.github.ivbaranov.mli.MaterialLetterIcon;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.Random;

import Persistance.Preference;
import Persistance.Station;
import Persistance.TripLineStation;
import Persistance.User;
import ViewModels.LineViewModel;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by souha on 18/04/2018.
 */

public class PreferenceAdapter extends BaseAdapter implements Filterable {
    public static List<Integer> selectedPrefs;
    Context context;
    public ArrayList<LineViewModel> items = new ArrayList<>();
    Realm realm;
    PreferenceAdapter adapter ;
    static int[] colors;
    PreferenceFilter filter;
    public PreferenceAdapter(Context context, ArrayList<LineViewModel> items) {
        this.context = context;
        this.items = items;
        adapter = this;
        realm = Realm.getDefaultInstance();
        if (PreferenceAdapter.selectedPrefs == null) {
            PreferenceAdapter.selectedPrefs = new ArrayList<>();
        }
        // delete Prefs
        PreferenceAdapter.selectedPrefs.clear();
        // initialize elements
        User currentUser = realm.where(User.class).findFirst();
        RealmResults<Preference> prefs = realm.where(Preference.class)
                .equalTo("user.userId", currentUser.getUserId())
                .greaterThan("line.lineId", 0)
                .findAll();
        for (Preference pre : prefs
                ) {
            PreferenceAdapter.selectedPrefs.add(pre.getLine().getLineId());
        }

    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public LineViewModel getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        LineViewModel item = items.get(i);
        if (item != null)
            return item.getLineId();
        return -1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.station_card, viewGroup, false);
        }

        final LineViewModel s = (LineViewModel) this.getItem(i);

        TextView propTxt = (TextView) view.findViewById(R.id.station_name);
        final MaterialLetterIcon myLetterIcon = (MaterialLetterIcon) view.findViewById(R.id.station_icon);
        if (CustomAdapter.colors == null || CustomAdapter.colors.length == 0)
            CustomAdapter.colors = view.getResources().getIntArray(R.array.letterShapeColors);
        int randomAndroidColor = CustomAdapter.colors[new Random().nextInt(CustomAdapter.colors.length)];


        // Test on ItemIds
        if (PreferenceAdapter.selectedPrefs.contains(s.getLineId())){
            view.setBackgroundResource(R.drawable.card_selected_done);
        }
        else{
            view.setBackgroundResource(0);
        }
        myLetterIcon.setShapeColor(randomAndroidColor);
        String id = Integer.toString(s.getLineId());
        String name = s.getLineName();
        myLetterIcon.setLetter(name);
        propTxt.setText(name);
        // Prompt a Dialog
        final View finalView = view;
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceAdapter.selectedPrefs.contains(s.getLineId())) {
                    int currentIndex = PreferenceAdapter.selectedPrefs.indexOf(s.getLineId());
                    PreferenceAdapter.selectedPrefs.remove(currentIndex);
                    finalView.setBackgroundResource(0);
                } else {
                    PreferenceAdapter.selectedPrefs.add(s.getLineId());
                    finalView.setBackgroundResource(R.drawable.card_selected_done);
                }


            }
        });

        return view;
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new PreferenceFilter(items,this);
        }

        return filter;
    }
}
