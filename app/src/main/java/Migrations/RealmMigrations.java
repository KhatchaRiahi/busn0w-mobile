package Migrations;

import android.util.Log;

import Persistance.Line;
import Persistance.Preference;
import Persistance.User;
import io.realm.DynamicRealm;
import io.realm.DynamicRealmObject;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by souha on 15/04/2018.
 */

public class RealmMigrations implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        final RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            schema.create("User")
                    .addField("userId", int.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            Log.d("realm", "Set the unique key"); //this line never called in above codes.
                            obj.set("userId", 0);
                        }
                    });
            schema.get("User")
                    .addPrimaryKey("userId")
                    .addField("userName", String.class)
                    .addField("Token", String.class);
            schema.create("Preference")
                    .addField("preferenceId", int.class)
                    .transform(new RealmObjectSchema.Function() {
                        @Override
                        public void apply(DynamicRealmObject obj) {
                            Log.d("realm", "Set the unique key"); //this line never called in above codes.
                            obj.set("preferenceId", 0);
                        }
                    });

            schema.get("Preference").addPrimaryKey("preferenceId")
                    .addField("localId", int.class)
                    .addRealmObjectField("line", schema.get("Line"))
                    .addRealmObjectField("user", schema.get("User"));
            oldVersion+=2;
        }
        if (oldVersion == 2) {
            schema.get("User")
                    .addField("email",String.class);
            oldVersion++;
        }
    }
}

