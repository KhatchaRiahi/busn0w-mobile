package com.example.souha.testinglayout;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import Migrations.RealmMigrations;
import Persistance.User;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObject;
import io.realm.RealmResults;

public class SplachScreen extends AppCompatActivity {
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splach_screen);
        //inti realm

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(3)
                .migration(new RealmMigrations())
                .build();
        Realm.setDefaultConfiguration(config);
        Window window = this.getWindow();

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.setStatusBarColor(ContextCompat.getColor(this, R.color.light_blue));

        ImageView imageView = (ImageView) findViewById(R.id.splach_scrren_logo);
        // fade Image
        ObjectAnimator animator = ObjectAnimator.ofFloat(imageView, View.ALPHA, 0.0f, 1.0f);
        animator.setDuration(2000);
        animator.start();
        // My Activity
        final Intent loginActivity = new Intent(this, LoginActivity.class);
        final Intent dashboard = new Intent(this, NavigationMenu.class);
        // Test If The User Is Already Signed In Then
        realm = Realm.getDefaultInstance();

        //User loggedInUser = realm.copyFromRealm(realm.where(User.class).findFirst());
        final Boolean userLoggedIn =realm.where(User.class).count() > 0 &&  realm.where(User.class).isNull("Token").count() == 0;
        // Thread Timer
        Thread launcher = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (userLoggedIn) {
                        startActivity(dashboard);
                    } else {
                        startActivity(loginActivity);
                    }
                }
            }
        };
        launcher.start();
    }
}
