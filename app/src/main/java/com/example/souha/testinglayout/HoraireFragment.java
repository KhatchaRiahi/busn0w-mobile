package com.example.souha.testinglayout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.lang.reflect.Array;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import CustomLayout.CustomAdapter;
import Persistance.Line;
import Persistance.Preference;
import Persistance.Station;
import Persistance.TripLineStation;
import ViewModels.LineViewModel;
import ViewModels.StationsViewModel;
import io.realm.Realm;
import io.realm.RealmResults;


public class HoraireFragment extends ListFragment{
    CustomAdapter adapter;
    ListView lv;
    private Realm realm;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_horaire, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //region RealmTest
        realm = Realm.getDefaultInstance();
        // create Some Stations If There is no stations

        // getAllStation
        // Display Stations
        //endregion
        lv= (ListView) getListView();
        //createStations(realm);
        adapter=new CustomAdapter(getActivity(),getStations(realm));
        lv.setAdapter(adapter);
    }
    private void createStations (Realm realm){
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                // Add a person

                Station station = realm.createObject(Station.class, 1);
                station.setDesignationFr("Station Test");
            }
        });
    }
    private ArrayList getStations(Realm realm){
        // Get User Preferences
        RealmResults<Preference> preferences = realm.where(Preference.class)
                .greaterThan("line.lineId",0)
                .findAll();
        Integer[] preferencesIds = new Integer[preferences.size()];
        int index =0 ;
        for(Preference pref : preferences){
            preferencesIds[index] = pref.getLine().getLineId();
            index++;
        }


        ArrayList<LineViewModel> toReturnList = new ArrayList<>();
        if (preferencesIds.length == 0)
            return toReturnList;
        RealmResults<Line> results = realm.where(Line.class)
                .distinct("lineName")
                .in("lineId",preferencesIds)
                .findAll();
        List<Line>  lines = realm.copyFromRealm(results);
        for(Line line : lines ){
            LineViewModel currentLine = new LineViewModel();
            currentLine.setLineId(line.getLineId());
            currentLine.setLineName(line.getLineName());
            toReturnList.add(currentLine);
        }
        return toReturnList;
    }
}
