package com.example.souha.testinglayout;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.pchmn.materialchips.ChipsInput;
import com.pchmn.materialchips.model.Chip;
import com.pchmn.materialchips.model.ChipInterface;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.xml.transform.Result;

import Persistance.Line;
import Persistance.LineStation;
import Persistance.Preference;
import Persistance.Station;
import Persistance.User;
import ViewModels.LineViewModel;
import io.realm.Realm;
import io.realm.RealmResults;


/**
 * A simple {@link Fragment} subclass.
 */
public class StationsPositionFragment extends Fragment implements OnMapReadyCallback {
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    Realm realm;
    ChipsInput chipsInput;
    List<Chip> contactList = new ArrayList<>();
    Map<Integer, Polyline> polyLines = new HashMap<Integer, Polyline>();
    Map<Integer,List<Marker>> markers = new HashMap<>();
    public StationsPositionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stations_position, container, false);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        realm = Realm.getDefaultInstance();
        if (mapFragment == null) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        User user = realm.where(User.class).findFirst();
        RealmResults<Preference> preferences = realm.where(Preference.class)
                .equalTo("user.userId", user.getUserId())
                .findAll();
        for (Preference pref : preferences
                ) {
            LineViewModel currentViewModel = new LineViewModel();
            try
            {
                contactList.add(new Chip(pref.getLine().getLineId(), pref.getLine().getLineName(), pref.getLine().getLineName()));
            }
            catch (Exception ex){
                Log.d("Exception ","Teest");
            }

        }
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        chipsInput = (ChipsInput) getActivity().findViewById(R.id.chips_input);
        // pass the ContactChip list
        chipsInput.setFilterableList(contactList);
        chipsInput.addChipsListener(new ChipsInput.ChipsListener() {
            @Override
            public void onChipAdded(ChipInterface chip, int newSize) {
                // chip added
                // newSize is the size of the updated selected chip list
                // manage The Map Ui
                // load The Stations Of This Line

                RealmResults<LineStation> lineStations = realm.where(LineStation.class)
                        .equalTo("line.lineId", (int) chip.getId())
                        .sort("position")
                        .findAll();
                // Add This Roads
                Station stationDepart = lineStations.get(0).getStation();
                Station stationArrivee = lineStations.get(lineStations.size() - 1).getStation();


                // Draw All Markers
                List<Marker> currentMarkers = new ArrayList<>();
                List<LatLng> positions = new ArrayList<>();
                for (LineStation lineStation :
                        lineStations) {
                    LatLng current = new LatLng(Double.parseDouble(lineStation.getStation().getLatitude()), Double.parseDouble(lineStation.getStation().getLongitude()));
                    positions.add(current);
                    MarkerOptions marqueur = new MarkerOptions().position(current)
                            .title(lineStation.getStation().getDesignationFr());
                    Marker localMarker = mMap.addMarker(marqueur);
                    currentMarkers.add(localMarker);

                }
                Polyline line = mMap.addPolyline(new PolylineOptions()
                        .addAll(positions)
                        .width(10)
                        .color(Color.parseColor("#05b1fb"))
                        .geodesic(true));
                polyLines.put((Integer) chip.getId(), line);
                markers.put((Integer) chip.getId(),currentMarkers);
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        new LatLng(Double.parseDouble(stationDepart.getLatitude()),
                                Double.parseDouble(stationDepart.getLongitude())), 10);
                mMap.animateCamera(location);
            }

            @Override
            public void onChipRemoved(ChipInterface chip, int newSize) {
                // chip removed
                // newSize is the size of the updated selected chip list
                Polyline line = polyLines.get((Integer) chip.getId());
                line.remove();
                polyLines.remove((Integer) chip.getId());
                // Remove Makers
                List<Marker> toDeleteMarkers = markers.get(chip.getId());
                for (int i = 0 ; i < toDeleteMarkers.size();i++)
                {
                    Marker el = toDeleteMarkers.get(i);
                    el.remove();

                }
                markers.remove(chip.getId());


            }

            @Override
            public void onTextChanged(CharSequence text) {
                // text changed
            }
        });
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // init Map Here
    }
}
