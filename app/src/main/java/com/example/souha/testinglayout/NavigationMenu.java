package com.example.souha.testinglayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.DumperPluginsProvider;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.dumpapp.DumperPlugin;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.w3c.dom.Text;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import Persistance.Line;
import Persistance.LineStation;
import Persistance.Preference;
import Persistance.Station;
import Persistance.TripLine;
import Persistance.TripLineStation;
import Persistance.TypeJour;
import Persistance.User;
import Services.APIClient;
import Services.APIInterface;
import Services.SyncManager;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NavigationMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    APIInterface apiInterface;
    Realm realm;
    ProgressDialog progressDialog;
    TextView loggedUser;
    boolean doubleBackToExitPressedOnce = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // The Top Notification bar Color
        Window window = this.getWindow();

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.setStatusBarColor(ContextCompat.getColor(this, R.color.light_blue));
        setContentView(R.layout.activity_navigation_menu);

        Realm.init(this);
        JodaTimeAndroid.init(this);
        realm = Realm.getDefaultInstance();
        User connectedUser = realm.where(User.class).findFirst();

        // Get The Layout

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        loggedUser = (TextView) headerView.findViewById(R.id.loggedUserName);
        loggedUser.setText(connectedUser.getUserName());
        // Progress Dialog Style Horizontal
        initProgressDialog();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_stations);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new StationsPositionFragment()).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sync) {
            // Sync here
            SyncManager sync = new SyncManager(realm, progressDialog);
            sync.loadStations();
            sync.loadLines();
            sync.loadTypeJours();
            sync.loadLineStations();
            sync.loadTripLines();
            sync.loadTripLineStations();
            sync.loadPreferences();
            return true;
        }
        else if (id == R.id.action_send){
            SyncManager sync = new SyncManager(realm, progressDialog);
            sync.sendPreferences();
        }
        else if (id == R.id.action_logout) {
            // Logout
            // Delete The Realm User
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(User.class).findFirst().deleteFromRealm();
                    realm.where(Station.class).findAll().deleteAllFromRealm();
                    realm.where(Line.class).findAll().deleteAllFromRealm();
                    realm.where(TypeJour.class).findAll().deleteAllFromRealm();
                    realm.where(LineStation.class).findAll().deleteAllFromRealm();
                    realm.where(TripLine.class).findAll().deleteAllFromRealm();
                    realm.where(TripLineStation.class).findAll().deleteAllFromRealm();
                    realm.where(Preference.class).findAll().deleteAllFromRealm();
                }
            });

            // Go To Login Page
            Intent loginActivity = new Intent(this, LoginActivity.class);
            startActivity(loginActivity);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_horaires) {
            // set the fragment to Load Content

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new HoraireFragment()).commit();
        } else if (id == R.id.nav_stations) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new StationsPositionFragment()).commit();
        } else if (id == R.id.nav_params) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, new ParamsFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100); // Progress Dialog Max Value
        progressDialog.setTitle("Chargement des données"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }


}
