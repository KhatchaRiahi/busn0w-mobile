package com.example.souha.testinglayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import Migrations.RealmMigrations;
import Persistance.Station;
import Persistance.User;
import Services.APIClient;
import Services.APIInterface;
import Services.SyncManager;
import ViewModels.TokenResponse;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Realm realm;
    private APIInterface apiInterface;
    private EditText userNameInput ;
    private EditText passwordInput;
    private Context context;
    boolean doubleBackToExitPressedOnce = false;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Window window = this.getWindow();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.setStatusBarColor(ContextCompat.getColor(this, R.color.blue));
        setContentView(R.layout.activity_login);

        // Init Componnets
        userNameInput = (EditText) findViewById(R.id.username);
        passwordInput = (EditText) findViewById(R.id.password);
        CardView cardView = (CardView) findViewById(R.id.loginButton);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get Elements From Input
                final String username = userNameInput.getText().toString();
                String password = passwordInput.getText().toString();
                // Block UI
                final ProgressDialog dialog=new ProgressDialog(context);
                dialog.setMessage("Log In ...");
                dialog.setCancelable(false);
                dialog.setInverseBackgroundForced(false);
                dialog.setIndeterminate(true);
                dialog.show();
                // Send Login Request
                Call<TokenResponse> call = apiInterface.getToken(username,password);
                call.enqueue(new Callback<TokenResponse>() {
                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        // Foreach Stations Try To Add Or Update

                        // if response Succeded
                            //Create User And SAve Token To this User
                        if(response.isSuccessful()){
                            int userId = response.body().getUser();
                            String key = response.body().getKey();

                            realm.beginTransaction();
                            User currentUser = realm.where(User.class).equalTo("userId",userId).findFirst();
                            if (currentUser == null){
                                currentUser = realm.createObject(User.class,userId);
                                currentUser.setUserName(username);
                                currentUser.setToken(key);
                            }else{
                                currentUser.setToken(key);
                                realm.insertOrUpdate(currentUser);
                            }
                            realm.commitTransaction();
                            final Intent dashboard = new Intent(context, NavigationMenu.class);
                            initProgressDialog();
                            SyncManager sync = new SyncManager(realm, progressDialog);
                            sync.loadStations();
                            sync.loadLines();
                            sync.loadTypeJours();
                            sync.loadLineStations();
                            sync.loadTripLines();
                            sync.loadTripLineStations();
                            sync.loadPreferences();
                            Thread launcher = new Thread() {
                                public void run() {
                                    try {
                                        sleep(2500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    } finally {
                                        dialog.dismiss();
                                        startActivity(dashboard);
                                    }
                                }
                            };
                            launcher.start();
                        }
                        else{
                            Snackbar.make(findViewById(R.id.loginContent) ,"Error Verify Your Info !", Snackbar.LENGTH_LONG)
                                    .show();
                            dialog.dismiss();
                        }



                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        call.cancel();
                        // Retry
                    }
                });


            }
        });
        TextView textView = (TextView) findViewById(R.id.registerForm);
        final Intent registerForm = new Intent(this,RegisterUser.class);
        textView.setOnClickListener(new View.OnClickListener(){
            public void onClick (View v){
               startActivity(registerForm);
            }
        });

        // event Login
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    private void initProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100); // Progress Dialog Max Value
        progressDialog.setTitle("Chargement des données"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    }
}
