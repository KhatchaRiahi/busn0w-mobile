package com.example.souha.testinglayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.google.android.gms.common.api.Api;

import Persistance.User;
import Services.APIClient;
import Services.APIInterface;
import ViewModels.TokenResponse;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterUser extends AppCompatActivity {
    EditText userNameInput;
    EditText emailInput;
    EditText passwordInput;
    EditText retypeInput;
    APIInterface apiInterface;
    Context context;
    Realm realm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        context = this;
        realm =Realm.getDefaultInstance();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.setStatusBarColor(ContextCompat.getColor(this, R.color.blue));
        setContentView(R.layout.activity_register_user);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        // Init Event Listener
        CardView cardView = (CardView) findViewById(R.id.cardSignIn);
        CardView registerCard = (CardView) findViewById(R.id.register);
        final Intent signInForm = new Intent(this, LoginActivity.class);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(signInForm);
            }
        });
        // Bind Element
        userNameInput = (EditText) findViewById(R.id.username);
        emailInput = (EditText) findViewById(R.id.email);
        passwordInput = (EditText) findViewById(R.id.password);
        retypeInput = (EditText) findViewById(R.id.retypedPassword);


        registerCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateData()){
                    Snackbar.make(findViewById(R.id.registerContent) ,"Please Verify Your Info !", Snackbar.LENGTH_LONG)
                            .show();
                }else{
                    // Make Api Call
                    String userName = userNameInput.getText().toString();
                    String email = emailInput.getText().toString();
                    String password = passwordInput.getText().toString();
                    final ProgressDialog dialog=new ProgressDialog(context);
                    dialog.setMessage("Register ...");
                    dialog.setCancelable(false);
                    dialog.setInverseBackgroundForced(false);
                    dialog.setIndeterminate(true);

                    Call<User> call = apiInterface.register(userName,password,email);
                    if(isNetworkAvailable()){
                        dialog.show();
                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                // Foreach Stations Try To Add Or Update

                                // if response Succeded
                                //Create User And SAve Token To this User

                                if(response.isSuccessful()){
                                    User currentUser = response.body();
                                    realm.beginTransaction();
                                    realm.insertOrUpdate(response.body());
                                    realm.commitTransaction();
                                    Intent dashboard = new Intent(context, NavigationMenu.class);
                                    startActivity(dashboard);
                                }
                                else{
                                    if(response.code() == 409){
                                        Snackbar.make(findViewById(R.id.registerContent) ,"User Exist !", Snackbar.LENGTH_LONG)
                                                .show();
                                    }
                                    else{
                                        Snackbar.make(findViewById(R.id.registerContent) ,"Request Error !", Snackbar.LENGTH_LONG)
                                                .show();
                                    }
                                }
                                dialog.dismiss();


                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                call.cancel();
                                // Retry
                                Snackbar.make(findViewById(R.id.registerContent) ,"Connection Error!", Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        });
                    }
                    else{
                        Snackbar.make(findViewById(R.id.registerContent) ,"There is no internet connection on your phone !", Snackbar.LENGTH_LONG)
                                .show();
                    }
                }
            }
        });

    }

    private Boolean validateData() {
        // UserName Not Empty
        String userName = userNameInput.getText().toString();
        if (userName.isEmpty())
            return false;
        String email = emailInput.getText().toString();
        if (email.isEmpty())
            return false;
        String password = passwordInput.getText().toString();
        if (password.isEmpty() || password.length() < 8)
            return false;
        String retypedPassword = retypeInput.getText().toString();
        if (!retypedPassword.equals(password))
            return false;

        return true;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
