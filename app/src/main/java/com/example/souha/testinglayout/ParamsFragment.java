package com.example.souha.testinglayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.support.v7.widget.SearchView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import CustomLayout.CustomAdapter;
import CustomLayout.PreferenceAdapter;
import Persistance.Line;
import Persistance.Preference;
import Persistance.User;
import ViewModels.LineViewModel;
import io.realm.Realm;
import io.realm.RealmResults;

public class ParamsFragment extends ListFragment {
    PreferenceAdapter adapter;
    Realm realm;
    ListView lv;
    SearchView sv;
    Button sendButton;

    public ParamsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_params, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //region RealmTest
        sv = getActivity().findViewById(R.id.mSearch);
        sv.setIconifiedByDefault(false);
        realm = Realm.getDefaultInstance();
        lv = getListView();
        adapter = new PreferenceAdapter(getActivity(), getLines(realm));
        lv.setAdapter(adapter);
        lv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        setListAdapter(adapter);
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                adapter.getFilter().filter(query);
                return false;
            }
        });

        // set Button event Listener

        sendButton = getActivity().findViewById(R.id.saveChanges);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show Loading Bar !
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.where(Preference.class).findAll().deleteAllFromRealm();
                    }
                });
                final ProgressDialog dialog = new ProgressDialog(getActivity());
                dialog.setMessage("Saving Changes ...");
                dialog.setCancelable(false);
                dialog.setInverseBackgroundForced(false);
                dialog.setIndeterminate(true);
                dialog.show();

                // clear all prefs and then add new prefs

                final User user = realm.where(User.class).findFirst();
                int currentIndex =1;
                // get filteredItems and add it to DataBase
                final List<Integer> prefs = PreferenceAdapter.selectedPrefs;

                for (final Integer item :
                        prefs
                        ) {
                    // Create New Preference
                    realm.beginTransaction();

                    Preference currentPref = realm.createObject(Preference.class, currentIndex);
                    currentPref.setUser(user);
                    currentPref.setLine(realm.where(Line.class).equalTo("lineId", item.intValue()).findFirst());
                    currentPref.setLocalId(currentIndex);
                    realm.commitTransaction();
                    currentIndex++;
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, new HoraireFragment()).commit();
                    }
                }, 1000);
            }
        });
    }

    private ArrayList getLines(Realm realm) {
        // Get All Lines
        ArrayList<LineViewModel> toReturnList = new ArrayList<>();
        RealmResults<Line> results = realm.where(Line.class)
                .distinct("lineName")
                .findAll();
        List<Line> lines = realm.copyFromRealm(results);
        for (Line line : lines) {
            LineViewModel currentLine = new LineViewModel();
            currentLine.setLineId(line.getLineId());
            currentLine.setLineName(line.getLineName());
            toReturnList.add(currentLine);
        }
        return toReturnList;
    }

}
