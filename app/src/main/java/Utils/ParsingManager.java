package Utils;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by souha on 12/04/2018.
 */

public class ParsingManager {
    public static Date parser(String timeSpan) {
        String[] values = timeSpan.split(":");
        int hours = Integer.parseInt(values[0]);
        int minutes = Integer.parseInt(values[1]);
        int seconds = Integer.parseInt(values[2]);
        DateTime myDateTime = new DateTime(2018, 1, 1, hours, minutes, seconds);
        return myDateTime.toDate();
    }
}
