package Persistance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Type;
import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by souha on 08/04/2018.
 */

public class TripLine extends RealmObject {
    //region Properties
    @PrimaryKey
    @Expose
    @SerializedName("tripLineId")
    private int tripLineId;
    private Date heureDepart;
    private Date heureArrivee;
    @SerializedName("line")
    private Line line;
    private TypeJour typeJour;
    @Ignore
    @Expose
    @SerializedName("heureDepart")
    private String heureDepartString;
    @Ignore
    @Expose
    @SerializedName("heureArrivee")
    private String heureArriveeString;

    @Expose
    @Ignore
    @SerializedName("line")
    private String lineId;

    @Expose
    @Ignore
    @SerializedName("typeJour")
    private String typeJourId;
    //endregion

    //region Getters

    public String getTypeJourId() {
        return typeJourId;
    }

    public String getHeureDepartString() {
        return heureDepartString;
    }

    public String getHeureArriveeString() {
        return heureArriveeString;
    }

    public String getLineId() {
        return lineId;
    }

    public int getTripLineId() {
        return tripLineId;
    }

    public Date getHeureDepart() {
        return heureDepart;
    }

    public Date getHeureArrivee() {
        return heureArrivee;
    }

    public Line getLine() {
        return line;
    }

    public TypeJour getTypeJour() {
        return typeJour;
    }

    //endregion
    //region Setters

    public void setTypeJourId(String typeJourId) {
        this.typeJourId = typeJourId;
    }

    public void setHeureDepartString(String heureDepartString) {
        this.heureDepartString = heureDepartString;
    }

    public void setHeureArriveeString(String heureArriveeString) {
        this.heureArriveeString = heureArriveeString;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public void setTripLineId(int tripLineId) {
        this.tripLineId = tripLineId;
    }

    public void setHeureDepart(Date heureDepart) {
        this.heureDepart = heureDepart;
    }

    public void setHeureArrivee(Date heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public void setTypeJour(TypeJour typeJour) {
        this.typeJour = typeJour;
    }

    //endregion
}
