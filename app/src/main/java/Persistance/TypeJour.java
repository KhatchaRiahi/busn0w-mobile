package Persistance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by souha on 08/04/2018.
 */

public class TypeJour extends RealmObject {
    @PrimaryKey
    @SerializedName("typeJourId")
    @Expose
    private int typeJourId;
    @SerializedName("designationTypeJour")
    @Expose
    private String designationTypeJour;

    //region Getters

    public int getTypeJourId() {
        return typeJourId;
    }

    public String getDesignationTypeJour() {
        return designationTypeJour;
    }

    //endregion
    //region Setters

    public void setTypeJourId(int typeJourId) {
        this.typeJourId = typeJourId;
    }

    public void setDesignationTypeJour(String designationTypeJour) {
        this.designationTypeJour = designationTypeJour;
    }

    //endregion
}
