package Persistance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.meta.Exclusive;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by souha on 08/04/2018.
 */

public class LineStation extends RealmObject {
    @PrimaryKey
    @Expose
    @SerializedName("lineStationId")
    private int lineStationId;
    @Expose
    @SerializedName("position")
    private int position;
    private Line line;
    private Station station;
    @Ignore
    @SerializedName("station")
    @Expose
    private String stationId;
    @Ignore
    @SerializedName("line")
    @Expose
    private String lineId;
    //region Getters

    public String getStationId() {
        return stationId;
    }

    public String getLineId() {
        return lineId;
    }

    public int getLineStationId() {
        return lineStationId;
    }

    public int getPosition() {
        return position;
    }

    public Line getLine() {
        return line;
    }

    public Station getStation() {
        return station;
    }

    //endregion
    //region Setters

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public void setLineStationId(int lineStationId) {
        this.lineStationId = lineStationId;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    //endregion
}
