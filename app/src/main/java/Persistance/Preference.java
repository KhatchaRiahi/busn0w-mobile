package Persistance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

/**
 * Created by souha on 15/04/2018.
 */

public class Preference extends RealmObject {
    //region Porps

    @PrimaryKey
    @Expose
    @SerializedName("preferenceId")
    private int preferenceId;
    @Expose
    private int localId;
    @Expose
    @SerializedName("line")
    @Ignore
    private String lineId;
    @SerializedName("user")
    @Expose
    @Ignore
    private String userId;

    private Line line;
    private User user;
    //endregion

    //region getters

    public int getPreferenceId() {
        return preferenceId;
    }

    public int getLocalId() {
        return localId;
    }

    public String getLineId() {
        return lineId;
    }

    public String getUserId() {
        return userId;
    }

    public Line getLine() {
        return line;
    }

    public User getUser() {
        return user;
    }

    //endregion

    //region setters

    public void setPreferenceId(int preferenceId) {
        this.preferenceId = preferenceId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public void setUser(User user) {
        this.user = user;
    }


    //endregion


}
