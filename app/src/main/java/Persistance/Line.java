package Persistance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by souha on 08/04/2018.
 */

public class Line extends RealmObject  {
    @PrimaryKey
    @SerializedName("lineId")
    @Expose
    private int lineId ;
    @SerializedName("lineName")
    @Expose
    private String lineName;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("nombreStations")
    @Expose
    private int nombreStations ;
    private Station stationDepart;

    private Station stationArrivee;
    @SerializedName("stationDepart")
    @Ignore
    @Expose
    private String stationDepartId;
    @SerializedName("stationArrivee")
    @Ignore
    @Expose
    private String stationArriveeId;

    //region Getters

    public String getStationDepartId() {
        return stationDepartId;
    }

    public String getStationArriveeId() {
        return stationArriveeId;
    }

    public int getLineId() {
        return lineId;
    }

    public String getLineName() {
        return lineName;
    }

    public int getStatus() {
        return status;
    }

    public int getNombreStations() {
        return nombreStations;
    }

    public Station getStationDepart() {
        return stationDepart;
    }

    public Station getStationArrivee() {
        return stationArrivee;
    }
    //endregion
    //region Setters

    public void setStationDepartId(String  stationDepartId) {
        this.stationDepartId = stationDepartId;
    }

    public void setStationArriveeId(String stationArriveeId) {
        this.stationArriveeId = stationArriveeId;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setNombreStations(int nombreStations) {
        this.nombreStations = nombreStations;
    }

    public void setStationDepart(Station stationDepart) {
        this.stationDepart = stationDepart;
    }

    public void setStationArrivee(Station stationArrivee) {
        this.stationArrivee = stationArrivee;
    }

    //endregion
}
