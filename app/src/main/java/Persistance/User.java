package Persistance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by souha on 15/04/2018.
 */

public class User extends RealmObject {

    //region props
    @PrimaryKey
    @Expose
    @SerializedName("id")
    private int userId;
    @Expose
    @SerializedName("username")
    private String userName;
    @Expose
    @SerializedName("token")
    private String Token;
    @SerializedName("email")
    @Expose
    private String email;
    //endregion

    //region getters


    public String getEmail() {
        return email;
    }

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getToken() {
        return Token;
    }

    //endregion

    //region Setters

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setToken(String token) {
        Token = token;
    }

    //endregion
}
