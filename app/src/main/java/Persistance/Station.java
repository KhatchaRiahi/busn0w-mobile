package Persistance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.LinkingObjects;
import io.realm.annotations.PrimaryKey;

/**
 * Created by souha on 08/04/2018.
 */

public class Station extends RealmObject{

    //region Propreties
    @PrimaryKey
    @SerializedName("stationId")
    @Expose
    private int stationId ;
    @SerializedName("designationFr")
    @Expose
    private String designationFr ;
    @SerializedName("designationAr")
    @Expose
    private String designationAr;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("stationLevel")
    @Expose
    private int stationLevel;
    @SerializedName("status")
    @Expose
    private int status;

    //region dependencie
    @LinkingObjects("station")
    private final RealmResults<TripLineStation> tripLineStations = null;
    //endregion

    //endregion
    //region Getters
    public int getStationId() {
        return stationId;
    }

    public String getDesignationFr() {
        return designationFr;
    }

    public String getDesignationAr() {
        return designationAr;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public int getStationLevel() {
        return stationLevel;
    }

    public int getStatus() {
        return status;
    }
    //endregion
    //region Setters

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public void setDesignationFr(String designationFr) {
        this.designationFr = designationFr;
    }

    public void setDesignationAr(String designationAr) {
        this.designationAr = designationAr;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setStationLevel(int stationLevel) {
        this.stationLevel = stationLevel;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    //endregion
    //region Constructors

    public Station(int stationId, String designationFr, String designationAr, String longitude, String latitude, int stationLevel, int status) {
        this.stationId = stationId;
        this.designationFr = designationFr;
        this.designationAr = designationAr;
        this.longitude = longitude;
        this.latitude = latitude;
        this.stationLevel = stationLevel;
        this.status = status;
    }

    public Station() {
    }
    //endregion
}
