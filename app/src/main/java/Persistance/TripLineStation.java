package Persistance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by souha on 08/04/2018.
 */

public class TripLineStation extends RealmObject {
    //region Propreties
    @PrimaryKey
    @Expose
    @SerializedName("tripLineStationId")
    private int tripLineStationId;
    @Expose
    @SerializedName("position")
    private int position;

    private TripLine tripLine;

    private Station station;
    private Date heurePassage;
    @Ignore
    @Expose
    @SerializedName("station")
    private String stationId ;
    @Ignore
    @Expose
    @SerializedName("heurePassage")
    private String heurePassageString;
    @Ignore
    @Expose
    @SerializedName("tripLine")
    private String tripLineId;

    //endregion

    //region Getters

    public String getTripLineId() {
        return tripLineId;
    }

    public String getStationId() {
        return stationId;
    }

    public String getHeurePassageString() {
        return heurePassageString;
    }

    public int getTripLineStationId() {
        return tripLineStationId;
    }

    public int getPosition() {
        return position;
    }

    public TripLine getTripLine() {
        return tripLine;
    }

    public Station getStation() {
        return station;
    }

    public Date getHeurePassage() {
        return heurePassage;
    }

    //endregion
    //region Setters

    public void setTripLineId(String tripLineId) {
        this.tripLineId = tripLineId;
    }

    public void setStationId(String  stationId) {
        this.stationId = stationId;
    }

    public void setHeurePassageString(String heurePassageString) {
        this.heurePassageString = heurePassageString;
    }

    public void setTripLineStationId(int tripLineStationId) {
        this.tripLineStationId = tripLineStationId;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setTripLine(TripLine tripLine) {
        this.tripLine = tripLine;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public void setHeurePassage(Date heurePassage) {
        this.heurePassage = heurePassage;
    }

    //endregion
}
