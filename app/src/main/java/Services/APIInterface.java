package Services;

import java.util.List;

import Persistance.Line;
import Persistance.LineStation;
import Persistance.Preference;
import Persistance.Station;
import Persistance.TripLine;
import Persistance.TripLineStation;
import Persistance.TypeJour;
import Persistance.User;
import ViewModels.TokenResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by souha on 08/04/2018.
 */

public interface APIInterface {
    @GET("/api/stations")
    Call<List<Station>> getStations();

    @GET("/api/lines")
    Call<List<Line>> getLines();

    @GET("/api/typejours")
    Call<List<TypeJour>> getTypeJours();

    @GET("/api/linestation")
    Call<List<LineStation>> getLineStations();

    @GET("/api/triplines")
    Call<List<TripLine>> getTripLines();

    @GET("/api/triplinestation")
    Call<List<TripLineStation>> getTripLineStations();


    @GET("/api/preferences")
    Call<List<Preference>> getPreference(@Header("Authorization") String authorizationPrams);

    @POST("/auth/getToken/")
    @FormUrlEncoded
    Call<TokenResponse> getToken(@Field("username") String userName,@Field("password") String password);

    @POST("/auth/register/")
    @FormUrlEncoded
    Call<User> register(@Field("username") String userName,@Field("password") String password,@Field("email") String email);

    @POST("/api/preferences")
    Call<List<Preference>> postPreference(@Header("Authorization") String authorizationPrams,@Body List<Preference> preferences);

}
