package Services;

import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.ProgressBar;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import Persistance.Line;
import Persistance.LineStation;
import Persistance.Preference;
import Persistance.Station;
import Persistance.TripLine;
import Persistance.TripLineStation;
import Persistance.TypeJour;
import Persistance.User;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static Utils.ParsingManager.parser;

/**
 * Created by souha on 12/04/2018.
 */

public class SyncManager {
    private Realm realm;
    private static ProgressDialog progressDialog;
    private APIInterface apiInterface;

    public SyncManager(Realm realm, ProgressDialog progressDialog) {
        this.realm = realm;
        this.progressDialog = progressDialog;
        this.apiInterface = APIClient.getClient().create(APIInterface.class);
    }

    //region loaders
    public void loadStations() {
        progressDialog.setMessage("Stations");
        progressDialog.setProgress(0);// Setting Message
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        //Popup a Dialog And Loading Bar With Some Notification
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<Station>> call = apiInterface.getStations();
        call.enqueue(new Callback<List<Station>>() {
            @Override
            public void onResponse(Call<List<Station>> call, Response<List<Station>> response) {
                // Foreach Stations Try To Add Or Update
                realm.beginTransaction();
                int count = response.body().size();
                int index = 0;
                int result = 0;
                for (Station station : response.body()) {
                    realm.insertOrUpdate(station);
                    index++;
                    result = (index * 100 / count);
                    progressDialog.setProgress(result);

                }
                realm.commitTransaction();
            }

            @Override
            public void onFailure(Call<List<Station>> call, Throwable t) {
                call.cancel();
                // Retry
            }
        });
    }

    public void loadLines() {
        progressDialog.setMessage("Lines");
        progressDialog.setProgress(0);// Setting Message
        progressDialog.show();
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        //Popup a Dialog And Loading Bar With Some Notification
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<Line>> call = apiInterface.getLines();
        call.enqueue(new Callback<List<Line>>() {
            @Override
            public void onResponse(Call<List<Line>> call, Response<List<Line>> response) {
                // Foreach Stations Try To Add Or Update
                realm.beginTransaction();
                int count = response.body().size();
                int index = 0;
                int result = 0;
                for (Line line : response.body()) {
                    Station currentStationDepart = realm
                            .where(Station.class)
                            .equalTo("stationId", Integer.parseInt(line.getStationDepartId()))
                            .findFirst();
                    Station currentStationArrivee = realm
                            .where(Station.class)
                            .equalTo("stationId", Integer.parseInt(line.getStationArriveeId()))
                            .findFirst();
                    line.setStationArrivee(currentStationArrivee);
                    line.setStationDepart(currentStationDepart);
                    realm.insertOrUpdate(line);
                    index++;
                    result = (index * 100 / count);
                    progressDialog.setProgress(result);

                }
                realm.commitTransaction();
            }

            @Override
            public void onFailure(Call<List<Line>> call, Throwable t) {
                call.cancel();
                // Retry
            }
        });
    }

    public void loadLineStations() {
        progressDialog.setMessage("Lines Stations");
        progressDialog.setProgress(0);// Setting Message
        progressDialog.show();
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        //Popup a Dialog And Loading Bar With Some Notification
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<LineStation>> call = apiInterface.getLineStations();
        call.enqueue(new Callback<List<LineStation>>() {
            @Override
            public void onResponse(Call<List<LineStation>> call, Response<List<LineStation>> response) {
                // Foreach Stations Try To Add Or Update
                realm.beginTransaction();
                int count = response.body().size();
                int index = 0;
                int result = 0;
                for (LineStation lineStation : response.body()) {
                    //Get Speciified Line And Station For Object
                    int stationId = Integer.parseInt(lineStation.getStationId());
                    int lineId = Integer.parseInt(lineStation.getLineId());
                    Line currentLine = realm.where(Line.class).equalTo("lineId", lineId).findFirst();
                    Station currentStation = realm.where(Station.class).equalTo("stationId", stationId).findFirst();
                    lineStation.setLine(currentLine);
                    lineStation.setStation(currentStation);
                    realm.insertOrUpdate(lineStation);
                    index++;
                    result = (index * 100 / count);
                    progressDialog.setProgress(result);

                }
                realm.commitTransaction();
            }

            @Override
            public void onFailure(Call<List<LineStation>> call, Throwable t) {
                call.cancel();
                // Retry
            }
        });
    }

    public void loadTypeJours() {
        progressDialog.setMessage("Types Jours");
        progressDialog.setProgress(0);// Setting Message
        progressDialog.show();
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        //Popup a Dialog And Loading Bar With Some Notification
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<TypeJour>> call = apiInterface.getTypeJours();
        call.enqueue(new Callback<List<TypeJour>>() {
            @Override
            public void onResponse(Call<List<TypeJour>> call, Response<List<TypeJour>> response) {
                // Foreach Stations Try To Add Or Update
                realm.beginTransaction();
                int count = response.body().size();
                int index = 0;
                int result = 0;
                for (TypeJour typeJour : response.body()) {
                    realm.insertOrUpdate(typeJour);
                    index++;
                    result = (index * 100 / count);
                    progressDialog.setProgress(result);

                }
                realm.commitTransaction();
            }

            @Override
            public void onFailure(Call<List<TypeJour>> call, Throwable t) {
                call.cancel();
                // Retry
            }
        });
    }

    public void loadTripLines() {
        progressDialog.setMessage("Trip Lines");
        progressDialog.setProgress(0);// Setting Message
        progressDialog.show();
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        //Popup a Dialog And Loading Bar With Some Notification
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<TripLine>> call = apiInterface.getTripLines();
        call.enqueue(new Callback<List<TripLine>>() {
            @Override
            public void onResponse(Call<List<TripLine>> call, Response<List<TripLine>> response) {
                // Foreach Stations Try To Add Or Update
                realm.beginTransaction();
                int count = response.body().size();
                int index = 0;
                int result = 0;
                for (TripLine tripLine : response.body()) {
                    int lineId = Integer.parseInt(tripLine.getLineId());
                    Line line = realm.where(Line.class).equalTo("lineId", lineId).findFirst();
                    int typeJourId = Integer.parseInt(tripLine.getTypeJourId());
                    TypeJour typeJour = realm.where(TypeJour.class).equalTo("typeJourId", typeJourId).findFirst();
                    tripLine.setTypeJour(typeJour);
                    tripLine.setLine(line);
                    String heureDepart = tripLine.getHeureDepartString();
                    String heureArrivee = tripLine.getHeureArriveeString();

                    tripLine.setHeureArrivee(parser(heureArrivee));
                    tripLine.setHeureDepart(parser(heureDepart));

                    // Parse Date Logic Here
                    realm.insertOrUpdate(tripLine);
                    index++;
                    result = (index * 100 / count);
                    progressDialog.setProgress(result);

                }
                realm.commitTransaction();
            }

            @Override
            public void onFailure(Call<List<TripLine>> call, Throwable t) {
                call.cancel();
                // Retry
                progressDialog.dismiss();
            }
        });
    }

    public void loadTripLineStations() {
        progressDialog.setMessage("Trip Lines Stations");
        progressDialog.setProgress(0);// Setting Message
        progressDialog.show();
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        //Popup a Dialog And Loading Bar With Some Notification
        apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<List<TripLineStation>> call = apiInterface.getTripLineStations();

        call.enqueue(new Callback<List<TripLineStation>>() {
            @Override
            public void onResponse(Call<List<TripLineStation>> call, Response<List<TripLineStation>> response) {
                // Foreach Stations Try To Add Or Update
                realm.beginTransaction();
                int count = response.body().size();
                int index = 0;
                int result = 0;
                for (TripLineStation tripLineStation : response.body()) {
                    int stationId = Integer.parseInt(tripLineStation.getStationId());
                    Station currentStation = realm.where(Station.class).equalTo("stationId", stationId).findFirst();
                    int tripLineId = Integer.parseInt(tripLineStation.getTripLineId());
                    TripLine currentTripLine = realm.where(TripLine.class).equalTo("tripLineId", tripLineId).findFirst();
                    Date currentHeure = parser(tripLineStation.getHeurePassageString());
                    tripLineStation.setStation(currentStation);
                    tripLineStation.setHeurePassage(currentHeure);
                    tripLineStation.setTripLine(currentTripLine);
                    realm.insertOrUpdate(tripLineStation);
                    index++;
                    result = (index * 100 / count);
                    progressDialog.setProgress(result);

                }
                progressDialog.dismiss();
                realm.commitTransaction();
            }

            @Override
            public void onFailure(Call<List<TripLineStation>> call, Throwable t) {
                call.cancel();
                // Retry
                progressDialog.dismiss();
            }
        });
    }
    //endregion

    //region userdataLoading
    public void loadPreferences() {
        progressDialog.setMessage("Preferences");
        progressDialog.setProgress(0);// Setting Message
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        //Popup a Dialog And Loading Bar With Some Notification
        apiInterface = APIClient.getClient().create(APIInterface.class);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Preference.class).findAll().deleteAllFromRealm();
            }
        });
        // Get Authorization Token
        String authorizationToken = "Token ";
        final User userConnected = realm.where(User.class).findFirst();
        authorizationToken += userConnected.getToken();
        Call<List<Preference>> call = apiInterface.getPreference(authorizationToken);
        call.enqueue(new Callback<List<Preference>>() {
            @Override
            public void onResponse(Call<List<Preference>> call, Response<List<Preference>> response) {
                // Foreach Stations Try To Add Or Update
                realm.beginTransaction();
                int count = response.body().size();
                int index = 0;
                int result = 0;
                for (Preference preference : response.body()) {
                    int lineId = Integer.parseInt(preference.getLineId());
                    Line currentLine = realm.where(Line.class)
                            .equalTo("lineId", lineId)
                            .findFirst();
                    preference.setLine(currentLine);
                    preference.setUser(userConnected);
                    realm.insertOrUpdate(preference);
                    index++;
                    result = (index * 100 / count);
                    progressDialog.setProgress(result);
                }
                realm.commitTransaction();
            }

            @Override
            public void onFailure(Call<List<Preference>> call, Throwable t) {
                call.cancel();
                // Retry
            }
        });
    }

    public void sendPreferences() {
        progressDialog.setMessage("Sending Preferences");
        progressDialog.setProgress(0);// Setting Message
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        //Popup a Dialog And Loading Bar With Some Notification
        apiInterface = APIClient.getClient().create(APIInterface.class);

        // Get Authorization Token
        String authorizationToken = "Token ";
        final User userConnected = realm.where(User.class).findFirst();
        List<Preference> preferences = realm.copyFromRealm(realm.where(Preference.class)
                .greaterThan("line.lineId", 0)
                .greaterThan("user.userId", 0)
                .findAll());
        for (Preference pref : preferences
                ) {
            pref.setLineId(Integer.toString(pref.getLine().getLineId()));
            pref.setUserId(Integer.toString(pref.getUser().getUserId()));
        }
        authorizationToken += userConnected.getToken();
        Call<List<Preference>> call = apiInterface.postPreference(authorizationToken, preferences);
        call.enqueue(new Callback<List<Preference>>() {
            @Override
            public void onResponse(Call<List<Preference>> call, Response<List<Preference>> response) {
                // Foreach Stations Try To Add Or Update
                Log.d("Recieved ", "Success");
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<Preference>> call, Throwable t) {
                call.cancel();
                // Retry
                progressDialog.dismiss();
            }
        });
    }
    //endregion
}
