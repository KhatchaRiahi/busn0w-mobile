package ViewModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by souha on 15/04/2018.
 */

public class TokenResponse {
    @Expose
    @SerializedName("key")
    private String Key;
    @Expose
    @SerializedName("user")
    private int user;
    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}
