package ViewModels;

/**
 * Created by souha on 23/03/2018.
 */

public class StationsViewModel {
    private int stationId ;
    private String stationName;

    public String getStationName() {
        return stationName;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

}
