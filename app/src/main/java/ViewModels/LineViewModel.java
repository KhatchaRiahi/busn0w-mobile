package ViewModels;

/**
 * Created by souha on 18/04/2018.
 */

public class LineViewModel {
    //region props
    private int lineId;
    private String lineName;
    private String lastHeure = "00:00";
    private Boolean isHighlited = false;
    //endregion

    //region getters

    public Boolean getHighlited() {
        return isHighlited;
    }

    public int getLineId() {
        return lineId;
    }

    public String getLineName() {
        return lineName;
    }

    public String getLastHeure() {
        return lastHeure;
    }

    //endregion

    //region setters

    public void setHighlited(Boolean highlited) {
        isHighlited = highlited;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public void setLastHeure(String lastHeure) {
        this.lastHeure = lastHeure;
    }

    //endregion
}
