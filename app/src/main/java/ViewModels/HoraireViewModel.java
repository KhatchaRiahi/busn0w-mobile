package ViewModels;

/**
 * Created by souha on 23/03/2018.
 */

public class HoraireViewModel {
    private int horaireId ;
    private String heure ;

    public HoraireViewModel() {
    }

    public HoraireViewModel(int horaireId, String heure) {
        this.horaireId = horaireId;
        this.heure = heure;
    }

    public int getHoraireId() {
        return horaireId;
    }

    public String getHeure() {
        return heure;
    }

    public void setHoraireId(int horaireId) {
        this.horaireId = horaireId;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }
}
